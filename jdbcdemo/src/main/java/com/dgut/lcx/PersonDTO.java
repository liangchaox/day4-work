package com.dgut.lcx;

import javax.json.bind.annotation.JsonbDateFormat;
import javax.json.bind.annotation.JsonbNumberFormat;
import javax.json.bind.annotation.JsonbProperty;
import javax.json.bind.annotation.JsonbTransient;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * @author liang cx
 * @date 2019/11/28 15:56
 */


public class PersonDTO {

    @JsonbProperty("name")
    private String name;
    @JsonbProperty("age")
    private int age;


    // 为了使用jsonb,必须有一个空的默认构造方法
    public PersonDTO() {

    }

    public PersonDTO(String name, int age) {
        this.name = name;
        this.age = age;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
