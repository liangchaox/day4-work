package com.dgut.lcx.databaseCon;

import com.dgut.lcx.PersonDTO;

import javax.xml.transform.Result;
import java.sql.*;

/**
 * @author liang cx
 * @date 2019/11/28 16:06
 */

public class DbUntil {
    private static final String URL = "jdbc:h2:C:\\Users\\lcx\\Desktop\\23811\\test;MODE=MYSQL;DB_CLOSE_DELAY=-1";

    private static final String NAME = "sa";

    private static final String PASSWORD = "sa";

    public static void main(String[] args){
        try (Connection conn = DriverManager.getConnection(URL, NAME, PASSWORD)) {
            Statement stmt = conn.createStatement();

            stmt.executeUpdate(
                    "CREATE TABLE IF NOT EXISTS `user` (" +
                            "  `name` varchar(100) NOT NULL," +
                            "  `age` int(11) NOT NULL," +
                            "  PRIMARY KEY (`name`)" +
                            ");"
            );


            ResultSet rs = stmt.executeQuery("select * from user");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
//    public static void add(PersonDTO personDTO){
//        try (Connection conn = DriverManager.getConnection(URL, NAME, PASSWORD)) {
//            Statement stmt = conn.createStatement();
//
//            String sql="insert into `user` (`name`,`age`) values ("+personDTO.getName()+", "+personDTO.getAge()+");";
//            stmt.executeUpdate(sql);
//            stmt.executeUpdate(
//                    "insert into `user` (`name`,`age`) values ('Jerry', 27);" +
//                            "insert into `user` (`name`,`age`) values ('Angel', 25);"
//            );
//
//            ResultSet rs = stmt.executeQuery("select * from user");//选择import java.sql.ResultSet;
//            String sql1="insert into `user`(`name`,`age`) values (?,?)";
//            String sql="insert into user values(?,?)";
//            PreparedStatement preparedStatement=conn.prepareStatement(sql);
//            preparedStatement.setString(1,personDTO.getName());
//            preparedStatement.setInt(2,personDTO.getAge());
//            preparedStatement.execute();
////            // 遍历每行记录
////            while (rs.next()) {
////                //如果结果集中有数据，就会循环打印出来
////                System.out.println(rs.getString("id") + "," + rs.getString("name") + "," + rs.getInt("age"));
////            }
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//    }
}
