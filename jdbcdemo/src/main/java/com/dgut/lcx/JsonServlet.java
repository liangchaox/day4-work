package com.dgut.lcx;

import com.dgut.lcx.databaseCon.DbUntil;

import javax.annotation.Resource;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.stream.Collectors;

/**
 * @author liang cx
 * @date 2019/11/28 15:52
 */

@WebServlet(urlPatterns = {"/json"})
public class JsonServlet extends HttpServlet {
    private static final String URL = "jdbc:h2:C:\\Users\\lcx\\Desktop\\23811\\test;MODE=MYSQL;DB_CLOSE_DELAY=-1";

    private static final String NAME = "sa";

    private static final String PASSWORD = "sa";

    // 获取前端post提交的json字符串，并转换为对象；再把对象转为json字符串，生成响应返回前端。
    // curl -v -H "Content-Type: application/json" http://localhost:8080/webapp-demo/json -X POST --data '{"name":"黎志雄","age":100}'
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try (Connection conn = DriverManager.getConnection(URL, NAME, PASSWORD)) {
            Jsonb jsonb = JsonbBuilder.create();
            String postJsonString = new BufferedReader(new InputStreamReader(req.getInputStream(),"UTF-8"))
                    .lines().collect(Collectors.joining(System.lineSeparator()));

            // 利用jsonb把json字符串转换为对象
            PersonDTO personDTO = jsonb.fromJson(postJsonString, PersonDTO.class);
//        PersonDTO personDTO = jsonb.fromJson("{\"姓名\":\"黎志雄\"}", PersonDTO.class);
//        DbUntil.add(personDTO);
            String sql="insert into user values(?,?)";
            PreparedStatement preparedStatement=conn.prepareStatement(sql);
            preparedStatement.setString(1,personDTO.getName());
            preparedStatement.setInt(2,personDTO.getAge());
            preparedStatement.execute();
            resp.setContentType("application/json");
            resp.setCharacterEncoding("UTF-8");
            String jsonPerson = "code\":0,\"msg\":\"操作成功";
            resp.getWriter().println(jsonPerson);
        }catch (SQLException e) {
            e.printStackTrace();
        }
    }
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        /**
         *
         */
    }
}
